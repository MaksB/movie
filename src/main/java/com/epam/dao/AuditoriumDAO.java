package com.epam.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.model.Auditorium;

public class AuditoriumDAO {

	private static Map<Long, Auditorium> db = new HashMap<Long, Auditorium>(); 
	private static long id;
	
	public static List<Auditorium> getAll(){
		return new ArrayList<Auditorium>(db.values());
	} 
	
	public static Auditorium getByName(String name){
		for ( Entry<Long, Auditorium> au: db.entrySet()) {
			if(au.getValue().getName().equals(name)){
				return au.getValue();
			}
		}
		
		return null;
	}

	public static Map<Long, Auditorium> getDb() {
		return db;
	}

	public static void setDb(Map<Long, Auditorium> db) {
		AuditoriumDAO.db = db;
	}

	public static long getId() {
		return id;
	}

	public static void setId(long id) {
		AuditoriumDAO.id = id;
	}
	
}
