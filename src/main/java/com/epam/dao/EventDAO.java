package com.epam.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Ticket;

public class EventDAO {
	
	private static Map<Long, Event> db = new HashMap<Long, Event>();
	private static Map<Event, Map<Date, Auditorium>> evAuditorium = new HashMap<Event, Map<Date, Auditorium>>();
	private static Map<Event,List<Ticket>> tickets = new HashMap<Event, List<Ticket>>();
	
	private static Long id;
	
	public static Event save(Event event){
		event.setId(++id);
		db.put(id, event);
		return event;
	}
	
	public static void remove(Long id){
		db.remove(id);
	}
	
	public static Event getByName(String name){
		for (Entry<Long, Event> event : db.entrySet()) {
			if(event.getValue().getName().equals(name)){
				return event.getValue();
			}
		}
		return null;
	}
	
	public static List<Event> getAll(){
		return new ArrayList<Event>(db.values());
	}
	
	public static void addAuditorium(Event event, Auditorium auditorium, Date date){
		if(evAuditorium.containsKey(event)){
			if(!evAuditorium.get(event).containsKey(date)){
			evAuditorium.get(event).put(date, auditorium);
			}
		}else{
			Map<Date, Auditorium> map = new HashMap<Date, Auditorium>();
			map.put(date, auditorium);
			evAuditorium.put(event, map);
		}
	}
	
	public static Map<Date, Auditorium> getAuditorium(Event event){
		return evAuditorium.get(event);
	}
	
	public static void addTicket(Event event, Ticket ticket){
		if (tickets.containsKey(event)) {
			tickets.get(event).add(ticket);
		}else{
			List<Ticket> list = new ArrayList<>();
			list.add(ticket);
			tickets.put(event, list);
		}
	}
	
	public static List<Ticket> getTicket(Event event){
		return tickets.get(event);
	}

	public static Map<Long, Event> getDb() {
		return db;
	}

	public static void setDb(Map<Long, Event> db) {
		EventDAO.db = db;
	}

	public static Long getId() {
		return id;
	}

	public static void setId(Long id) {
		EventDAO.id = id;
	}

	public static Map<Event, Map<Date, Auditorium>> getEvAuditorium() {
		return evAuditorium;
	}

	public static void setEvAuditorium(Map<Event, Map<Date, Auditorium>> evAuditorium) {
		EventDAO.evAuditorium = evAuditorium;
	}

	public static Map<Event, List<Ticket>> getTickets() {
		return tickets;
	}

	public static void setTickets(Map<Event, List<Ticket>> tickets) {
		EventDAO.tickets = tickets;
	}

	
	
	

}
