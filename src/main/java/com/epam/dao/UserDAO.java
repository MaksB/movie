package com.epam.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.model.Ticket;
import com.epam.model.User;

public class UserDAO {

	private static Map<Long, User> db = new HashMap<Long,User>();
	private static Long id;
	
	public static User save(User user){
		user.setId(++id);
		db.put(user.getId(), user);
		return user;
	}
	
	public static void remove(Long id){
		db.remove(id);
	}
	
	public static User getById(Long id){
		return db.get(id);
	}
	
	public static User getByEmail(String mail){
		for (Entry<Long, User> entry : db.entrySet()) {
			if(entry.getValue().getEmail().equals(mail)){
				return entry.getValue();
			}
		}
		return null;
	}
	
	public static List<User> getByName(String name){
		List<User> list = new ArrayList<User>();
		for (Entry<Long, User> entry : db.entrySet()) {
			if(entry.getValue().getName().equals(name)){
				list.add(entry.getValue());
			}
		}
		return list;
	}
	
	public static List<Ticket> getBookedTickets(Long id){
		return db.get(id).getTicket();
	}

	public static Map<Long, User> getDb() {
		return db;
	}

	public static void setDb(Map<Long, User> db) {
		UserDAO.db = db;
	}

	public static Long getId() {
		return id;
	}

	public static void setId(Long id) {
		UserDAO.id = id;
	}
	
	
}
