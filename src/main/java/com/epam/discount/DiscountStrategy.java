package com.epam.discount;

import java.util.Date;

import com.epam.model.Event;
import com.epam.model.User;

public interface DiscountStrategy {

 Double getDiscount(User user, Event event, Date date);
}
