package com.epam.discount.impl;

import java.util.Date;

import com.epam.discount.DiscountStrategy;
import com.epam.model.Event;
import com.epam.model.Ticket;
import com.epam.model.User;

public class BirthdayDiscount implements DiscountStrategy{


	public Double getDiscount(User user, Event event, Date date) {
		Double rate = 0.0; 
		if(user.getBirthday().getDay()==date.getDay()&& user.getBirthday().getMonth()==date.getMonth()){
			 for (Ticket ticket : user.getTicket()) {
				if(ticket.getEvent().equals(event)){
					int  price = ticket.getPrice();
					Double newPrice = price - price * 0.05;
					rate+=price * 0.05;
					ticket.setPrice(newPrice.intValue());
				}
			} 
		 }
		return rate;
	}
}
