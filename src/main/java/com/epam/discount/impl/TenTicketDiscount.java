package com.epam.discount.impl;

import java.util.Date;

import com.epam.discount.DiscountStrategy;
import com.epam.model.Event;
import com.epam.model.Ticket;
import com.epam.model.User;

public class TenTicketDiscount implements DiscountStrategy {

	public Double getDiscount(User user, Event event, Date date) {
		Double rate = 0.0; 
		int count = 0;
			 for (Ticket ticket : user.getTicket()) {
				if(ticket.getEvent().equals(event)){
					count++;
					if(count == 10){
						int  price = ticket.getPrice();
						Double newPrice = (double) (price / 2);
						rate+=price / 2;
						ticket.setPrice(newPrice.intValue());
						count = 0;
					}
					
				}
			} 
		return rate;
	}

}
