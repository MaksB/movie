package com.epam.main;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Rating;
import com.epam.model.Ticket;
import com.epam.model.User;
import com.epam.service.UserService;
import com.epam.service.AuditoriumService;
import com.epam.service.BookingService;
import com.epam.service.impl.DiscountService;
import com.epam.service.EventService;

public class App {

	private static ConfigurableApplicationContext ap;
	
	@Autowired
	private UserService userService;
	@Autowired
	private EventService eventService;
	@Autowired
	@Qualifier("auditorium1")
	private Auditorium auditorium;
	@Autowired
	private AuditoriumService auditoriumService;
	@Autowired
	private BookingService bookingService;
	@Autowired
	private DiscountService discountService;
	
	public static void main(String[] args) {
		ap = new ClassPathXmlApplicationContext("spring.xml");
		
		App app = (App) ap.getBean("app");
		System.out.println("++++++++++++++++++++User service+++++++++++++++++++++++++");
		
		User user = new User();
		user.setEmail("mqazw@mail.com");
		user.setName("User");
		user.setBirthday(new Date("1993/1/17"));
		
		System.out.println("Register: "+app.userService.register(user));
		
		System.out.println("Get by id: "+app.userService.getById(1L));
		
		System.out.println("Get user by email: "+app.userService.getUserByEmail("mqazw@mail.com"));
		
		System.out.println("Get user by name: ");
		app.userService.getUsersByName("User").forEach(System.out::println);
		
		System.out.println("Remove user:");
		app.userService.remove(3L);
		System.out.println("Get by id=3: "+app.userService.getById(3L));
		
		System.out.println("Get booked ticket: ");
		app.userService.getBookedTickets(2L).forEach(System.out::println);
		
		System.out.println("++++++++++++++++++++Event service+++++++++++++++++++++++++");
		
		Event event = new Event();
		event.setName("The revenant");
		event.setPrice(100);
		event.setRating(Rating.MID);
		List<Date> eventDate = new ArrayList<>();
		eventDate.add(new Date("2016/2/20"));
		eventDate.add(new Date("2016/2/19"));
		event.setDate(eventDate);
		
		System.out.println("Create event: "+app.eventService.create(event));
		
		System.out.println("Get by name: "+ app.eventService.getByName("Star wars"));
		
		System.out.println("Get All");
		app.eventService.getAll().forEach(System.out::println);
		
		System.out.println("Assigned auditorium");
		app.eventService.assignAuditorium(event, app.auditorium, new Date("2016/2/20"));
		app.eventService.getEvAuditorium(event).values().forEach(System.out::println);
		
		System.out.println("Get Next: "+app.eventService.getNextEvents(new Date("2016/2/20")));
		
		System.out.println("Get from to: "+ app.eventService.getForDateRange(new Date("2015/5/17"), new Date("2016/2/20")));
		
		
		System.out.println("Remove");
		app.eventService.remove(2L);
		app.eventService.getAll().forEach(System.out::println);
		
		System.out.println("++++++++++++++++++++Auditorium Service+++++++++++++++++++++++++");
		
		System.out.println("Get auditoriums ");
		app.auditoriumService.getAuditoriums().forEach(System.out::println);
		
		System.out.println("Get set number: "+app.auditoriumService.getSeatsNumber("Red"));
		
		System.out.println("Get vip sets: "+app.auditoriumService.getVipSeats("Red"));
		
		System.out.println("++++++++++++++++++++BookingService Service+++++++++++++++++++++++++");
		
		System.out.println("Get tickets for events: ");
		app.bookingService.getTicketsForEvent(app.eventService.getByName("Star wars"), new Date("2016/02/7")).forEach(System.out::println);
		
		System.out.println("Booked ticket ");
		Ticket ticket = new Ticket();
		ticket.setEvent(event);
		ticket.setIsVip(true);
		ticket.setPrice(200);
		ticket.setSeats(1);
		ticket.setDate(new Date("2016/2/20"));
		
		System.out.println("Bokked ticket: "+ app.bookingService.getBookedTicket(event));
		app.bookingService.bookTicket(user, ticket);
		System.out.println("Bokked ticket when user buy: ");
		app.bookingService.getBookedTicket(event).forEach(System.out::println);
		
		System.out.println("Get ticket price: "+app.bookingService.getTicketPrice(event, new Date("2016/2/20"), 3, user));
		
		System.out.println("++++++++++++++++++++Discount Service+++++++++++++++++++++++++");
		
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		
		System.out.println("Get discoutn: "+app.discountService.getDiscount(user, event, new Date("2016/2/20")));
		
	}

}
