package com.epam.service;

import java.util.List;

import com.epam.model.Auditorium;

public interface AuditoriumService {
	List<Auditorium> getAuditoriums();
	Integer getSeatsNumber(String name); 
	List getVipSeats(String name);
}
