package com.epam.service;

import java.util.Date;
import java.util.List;

import com.epam.model.Event;
import com.epam.model.Ticket;
import com.epam.model.User;

public interface BookingService {

  Double getTicketPrice(Event event, Date date, Integer seats, User user);
  void bookTicket(User user, Ticket ticket);
  List<Ticket> getTicketsForEvent(Event event, Date date);
  List<Ticket> getBookedTicket(Event event);
  void saveBookedTicket(Event evet, Ticket ticket);
	
}
