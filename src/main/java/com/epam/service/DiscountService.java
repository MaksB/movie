package com.epam.service;

import java.util.Date;
import java.util.List;

import com.epam.discount.DiscountStrategy;
import com.epam.model.Event;
import com.epam.model.User;

public interface DiscountService {
 
 Double getDiscount(User user, Event event, Date date);

}
