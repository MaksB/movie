package com.epam.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Ticket;

public interface EventService {

	Event create(Event event);
	void remove(Long id); 
	Event getByName(String name); 
	List<Event> getAll();
	void assignAuditorium(Event event, Auditorium auditorium, Date date);
	List<Event> getForDateRange(Date from, Date to);
	List<Event> getNextEvents(Date to);
	Map<Date, Auditorium> getEvAuditorium(Event event);
	
	
}
