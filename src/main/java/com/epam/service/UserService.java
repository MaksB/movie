package com.epam.service;

import java.util.List;

import com.epam.model.Ticket;
import com.epam.model.User;

public interface UserService {
	User register(User user);
	void remove(Long id);
	User getById(Long id);
	User getUserByEmail(String email);
	List<User> getUsersByName(String name);
	List<Ticket> getBookedTickets(Long id);

}
