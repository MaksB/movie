package com.epam.service.impl;

import java.util.List;

import com.epam.dao.AuditoriumDAO;
import com.epam.model.Auditorium;

public class AuditoriumService implements com.epam.service.AuditoriumService{

	public List<Auditorium> getAuditoriums() {
		return AuditoriumDAO.getAll();
	}

	public Integer getSeatsNumber(String name) {
		return AuditoriumDAO.getByName(name).getNumberOfSeats();
	}

	public List getVipSeats(String name) {
		return AuditoriumDAO.getByName(name).getVipSeats();
	}



}
