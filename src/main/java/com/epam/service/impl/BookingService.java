package com.epam.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.dao.EventDAO;
import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Rating;
import com.epam.model.Ticket;
import com.epam.model.User;

public class BookingService implements com.epam.service.BookingService {

	@Autowired
	private UserService userService;
	@Autowired
	private EventService eventService;
	@Autowired
	private DiscountService discountService;
	
	public Double getTicketPrice(Event event, Date date, Integer seats, User user) {
		double prace = event.getPrice();
		Auditorium auditorium = eventService.getEvAuditorium(event).get(date);
		if(event.getRating().equals(Rating.HIGH)){
			prace*=1.2;
		}
		if(auditorium.getVipSeats().contains(seats)){
			prace*=2;
		}
		
		return prace-discountService.getDiscount(user, event, date);
	}

	public void bookTicket(User user, Ticket ticket) {
		if(userService.getUserByEmail(user.getEmail())!=null){
		user.getTicket().add(ticket);
		}
		saveBookedTicket(ticket.getEvent(), ticket);
		
	}

	public List<Ticket> getTicketsForEvent(Event event, Date date) {
		List<Ticket> buyTicket = new ArrayList<Ticket>();
		for(Ticket ticket: getBookedTicket(event)){
			if( ticket.getDate().equals(date)){
				buyTicket.add(ticket);
			}
		}
		return buyTicket;
	}
	
	public List<Ticket> getBookedTicket(Event event){
		return EventDAO.getTicket(event);
	}
	
	public void saveBookedTicket(Event event, Ticket ticket){
		EventDAO.addTicket(event, ticket);
	}
	
	

}
