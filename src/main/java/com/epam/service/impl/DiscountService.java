package com.epam.service.impl;

import java.util.Date;
import java.util.List;

import com.epam.discount.DiscountStrategy;
import com.epam.model.Event;
import com.epam.model.User;

public class DiscountService implements com.epam.service.DiscountService {

	private List<DiscountStrategy> discount;

	public Double getDiscount(User user, Event event, Date date) {
		double dis = 0;
		for (DiscountStrategy discountStrategy : discount) {
			double newDis =discountStrategy.getDiscount(user, event, date);
			if(newDis > dis){
				dis=newDis;
			}
		}
		return dis;
	}

	public List<DiscountStrategy> getDiscount() {
		return discount;
	}

	public void setDiscount(List<DiscountStrategy> discount) {
		this.discount = discount;
	}
	
	
	
	
}
