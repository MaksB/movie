package com.epam.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.epam.dao.EventDAO;
import com.epam.model.Auditorium;
import com.epam.model.Event;

public class EventService implements com.epam.service.EventService{

	public Event create(Event event) {
		return EventDAO.save(event);
		
	}

	public void remove(Long id) {
		EventDAO.remove(id);
		
	}

	public Event getByName(String name) {
		return EventDAO.getByName(name);
	}

	public List<Event> getAll() {
		return EventDAO.getAll();
	}

	public void assignAuditorium(Event event, Auditorium auditorium, Date date) {
		EventDAO.addAuditorium(event, auditorium, date);
		
	}

	public List<Event> getForDateRange(Date from, Date to) {
		List<Event> events = new ArrayList<Event>();
		for (Event event : EventDAO.getAll()) {
			boolean isTo = false;
			for (Date date : event.getDate()) {
				if(from.getTime()<=date.getTime() && date.getTime()<to.getTime()){
					isTo=true;
				}
			}
			if(isTo){
			  events.add(event);
			}
		}
		return events;
	}

	public List<Event> getNextEvents(Date to) {
		List<Event> events = new ArrayList<Event>();
		for (Event event : EventDAO.getAll()) {
			boolean isTo = false;
			for (Date date : event.getDate()) {
				if(new Date().getTime()<=date.getTime() && date.getTime()<to.getTime()){
					isTo=true;
				}
			}
			if(isTo){
			  events.add(event);
			}
		}
		return events;
	}
	
	public Map<Date, Auditorium> getEvAuditorium(Event event) {
		return EventDAO.getAuditorium(event);
	}
	
	
	
}
