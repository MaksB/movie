package com.epam.service.impl;

import java.util.List;

import com.epam.dao.UserDAO;
import com.epam.model.Ticket;
import com.epam.model.User;

public class UserService implements com.epam.service.UserService{

	public User register(User user) {
		if(getUserByEmail(user.getEmail())==null){
			return UserDAO.save(user);
		}
		return null;
		
	}

	public void remove(Long id) {
		 UserDAO.remove(id);
		
	}

	public User getById(Long id) {
		return UserDAO.getById(id);
	}

	public User getUserByEmail(String email) {
		return UserDAO.getByEmail(email);
	}

	public List<User> getUsersByName(String name) {
		return UserDAO.getByName(name);
	}

	public List<Ticket> getBookedTickets(Long id) {
		return UserDAO.getBookedTickets(id);
	}

	
}
