package com.epam.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.epam.test.service.AuditoriumServiceTest;
import com.epam.test.service.BookingServiceTest;
import com.epam.test.service.EventServiceTest;
import com.epam.test.service.UserServiceTest;

@RunWith(Suite.class)
@SuiteClasses({UserServiceTest.class, AuditoriumServiceTest.class,
EventServiceTest.class, BookingServiceTest.class})
public class SuiteClass {

}
