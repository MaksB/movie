package com.epam.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.model.Auditorium;
import com.epam.service.AuditoriumService;
import com.epam.test.Base;
import static org.junit.Assert.*;

import java.util.List;
public class AuditoriumServiceTest extends Base{

	@Autowired
	private AuditoriumService auditoriumService;
	
	@Test
	public void getAuditoriums(){
		System.out.println("Get auditoriums ");
		List<Auditorium> auditoriums = auditoriumService.getAuditoriums();
		auditoriums.forEach(System.out::println);
		assertTrue(auditoriums.size()>0);
	}
	
	@Test
	public void getSeatsNumberTest(){
		System.out.println("Get set number: "+auditoriumService.getSeatsNumber("Red"));
		assertTrue(auditoriumService.getSeatsNumber("Red")>0);
	}
	
	@Test
	public void getVipSeatsTest(){
		System.out.println("Get vip sets: "+auditoriumService.getVipSeats("Red"));
		assertTrue(auditoriumService.getVipSeats("Red").size()>0);
	}
}
