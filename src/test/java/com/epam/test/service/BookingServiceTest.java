package com.epam.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Ticket;
import com.epam.model.User;
import com.epam.service.BookingService;
import com.epam.service.EventService;
import com.epam.test.Base;

public class BookingServiceTest extends Base{

	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private EventService eventService;
	@Autowired
	@Qualifier("event3")
	private Event event;
	@Autowired
	@Qualifier("ticket2")
	private Ticket ticket;
	@Autowired
	@Qualifier("User2")
	private User user;
	
	@Autowired
	@Qualifier("auditorium1")
	private Auditorium auditorium;
	
	@Test
	public void getTicketsForEventTest() {
		System.out.println("Get tickets for events: ");
		List<Ticket> tickets = bookingService.getTicketsForEvent(eventService.getByName("Star wars"), new Date("2016/02/7"));
		tickets.forEach(System.out::println);
		assertTrue(tickets.size()>0);
	}
	
	@Test
	public void bookedTicketTest(){
		System.out.println("Booked ticket ");

		System.out.println("Bokked ticket: "+ bookingService.getBookedTicket(event));

		bookingService.bookTicket(user, ticket);
		
		System.out.println("Bokked ticket when user buy: ");
		List<Ticket> tickets = bookingService.getBookedTicket(event);
		tickets.forEach(System.out::println);
		
		assertTrue(tickets.size()>0);
	}
	
	@Test
	public void getTicketPriceTest(){
		System.out.println("Assigned auditorium");
		eventService.assignAuditorium(event, auditorium, new Date("2016/2/20"));
		Double price =  bookingService.getTicketPrice(event, new Date("2016/2/20"), 3, user);
		System.out.println("Get ticket price: "+price);
		assertTrue(price == 200);
	}
	
}
