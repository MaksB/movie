package com.epam.test.service;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static org.junit.Assert.*;

import com.epam.model.Event;
import com.epam.model.Ticket;
import com.epam.model.User;
import com.epam.service.DiscountService;
import com.epam.test.Base;

public class DiscountServiceTest extends Base{

	@Autowired
	private DiscountService discountService;
	
	@Autowired
	@Qualifier("event3")
	private Event event;
	@Autowired
	@Qualifier("ticket2")
	private Ticket ticket;
	@Autowired
	@Qualifier("User2")
	private User user;
	
	@Test
	public void getDicount(){
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		user.getTicket().add(ticket);
		
		double discount = discountService.getDiscount(user, event, new Date("2016/2/20"));
		System.out.println("Get discoutn: "+discount);
		assertTrue(discount==100);
	}
}
