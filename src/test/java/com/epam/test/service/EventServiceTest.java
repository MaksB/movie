package com.epam.test.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static org.junit.Assert.*;

import com.epam.model.Auditorium;
import com.epam.model.Event;
import com.epam.model.Rating;
import com.epam.service.EventService;
import com.epam.test.Base;

public class EventServiceTest extends Base{

	@Autowired
	private EventService eventService;
	
	@Autowired
	@Qualifier("auditorium1")
	private Auditorium auditorium;
	
	@Test
	public void createTets(){
		Event event = new Event();
		event.setName("The revenant");
		event.setPrice(100);
		event.setRating(Rating.MID);
		List<Date> eventDate = new ArrayList<>();
		eventDate.add(new Date("2016/2/20"));
		eventDate.add(new Date("2016/2/19"));
		event.setDate(eventDate);
		Event newEvent = eventService.create(event);
		System.out.println("Create event: "+newEvent);
		assertEquals(event.getName(), newEvent.getName());
		
	}
	
	@Test
	public void getByNameTest(){
		String name = "Star wars";
		Event event = eventService.getByName(name);
		System.out.println("Get by name: "+ event);
		assertEquals(event.getName(), name);
	}
	
	@Test
	public void getAll(){
		System.out.println("Get All");
		List<Event> events = eventService.getAll();
		events.forEach(System.out::println);
		assertTrue(events.size()>0);
	}
	
	@Test
	public void assignedAuditorium(){
		Event event = new Event();
		event.setName("The revenant");
		event.setPrice(100);
		event.setRating(Rating.MID);
		List<Date> eventDate = new ArrayList<>();
		eventDate.add(new Date("2016/2/20"));
		eventDate.add(new Date("2016/2/19"));
		event.setDate(eventDate);
		System.out.println("Assigned auditorium");
		eventService.assignAuditorium(event, auditorium, new Date("2016/2/20"));
		eventService.getEvAuditorium(event).values().forEach(System.out::println);
		assertEquals(eventService.getEvAuditorium(event).values().size(), 1);
	}
	
	@Test
	public void removeTest(){
		System.out.println("Remove");
		eventService.remove(2L);
		eventService.getAll().forEach(System.out::println);
		assertEquals(null, eventService.getByName("Star wars2"));
	}
	
	
}
