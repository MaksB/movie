package com.epam.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


import com.epam.model.Ticket;
import com.epam.model.User;
import com.epam.service.UserService;
import com.epam.test.Base;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;



public class UserServiceTest extends Base{

	@Autowired
	private UserService userService;
	
	
	@Test
	public void getByIDTest(){
		Long id = 2L;
		User user =userService.getById(id);
		System.out.println("Get by id: "+user);
		assertEquals(id, user.getId());
		
	}
	
	@Test
	public void getUserByNameTest(){
		String name = "User";
		List<User> users =userService.getUsersByName(name);
		System.out.println("Get user by name: ");
		users.forEach(System.out::println);
		for (User user : users) {
			assertEquals(name, user.getName());
		}
		
	}
	
	@Test
	public void getUserByEmailTest(){
		String email = "user@mail.com";
		User user =userService.getUserByEmail(email);
		System.out.println("Get user by email: "+userService.getUserByEmail(email));
		assertEquals(email, user.getEmail());
	}
	
	@Test
	public void registerTest(){
		User user = new User();
		user.setEmail("mqazw@mail.com");
		user.setName("User");
		user.setBirthday(new Date("1993/1/17"));
		User newUser = userService.register(user);
		System.out.println("Register: "+ newUser);
		assertEquals(user.getEmail(), newUser.getEmail());
	}
	
	@Test
	public void getBookedTicketTest(){
		System.out.println("Get booked ticket: ");
		List<Ticket> ticket =userService.getBookedTickets(2L);
		ticket.forEach(System.out::println);
		assertTrue(ticket.size()>0);
		
	}
	@Test
	public void removeTest(){
		System.out.println("Remove user:");
		userService.remove(1L);
		System.out.println("Get by id=1: "+userService.getById(1L));
		assertEquals(null, userService.getById(1L));
	}
}
